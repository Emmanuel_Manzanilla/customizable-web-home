/**
 * Table module, define variables and functions to manage the main table (the table of sites) in the page
 */

import { setImageInput } from "./main.js"

// The numbers of rows and columns
// Is exported to used at saving the data
export var numOfRows = 0, numOfColumns = 0
// Flag that determines if the selected cell perform a normal action (false) or show the EDIT site menu (false)
var inEditSiteMode = false
// Object that will have the HTML element with the selected cell
var selectedCell

/**
 * Adds a row in the main table
 */
export function sitesTableAddRow() {
    let trTag = document.createElement("tr")
    trTag.id = "row" + numOfRows
    for (let i = 0; i < numOfColumns; ++i) {
        trTag.appendChild(getNewCell(numOfRows, i))
    }
    document.getElementById("sitesTable").appendChild(trTag)

    if (numOfRows == 1) {
        document.getElementById("preferencesDeleteRowButton").disabled = false
    }
    ++numOfRows
}
/**
 * Adds a column in the main table
 */
export function sitesTableAddColumn() {
    for (let i = 0; i < numOfRows; ++i) {
        document.getElementById("row" + i).appendChild(getNewCell(i, numOfColumns))
    }

    if (numOfColumns == 1) {
        document.getElementById("preferencesDeleteColumnButton").disabled = false
    }
    ++numOfColumns
}
/**
 * Creates and returns a new cell in the given coordinates
 * @param {int} x Horizontal coordinate of the new cell (X axis)
 * @param {int} y Vertical coordinate of the new cell (Y axis)
 * @returns {Object} HTML element with the cell
 */
function getNewCell(x, y) {
    let tdTag, pTag
    tdTag = document.createElement("td")
    tdTag.id = "cell" + x + y
    tdTag.link = ""
    pTag = document.createElement("p")
    pTag.className = "editMark"
    // The mark is language sensible
    pTag.textContent = browser.i18n.getMessage("editMarkLabel")

    tdTag.appendChild(pTag)
    tdTag.onclick = cellListener
    return tdTag
}
/**
 * Deletes a row in the main table
 */
export function sitesTableDeleteRow() {
    --numOfRows
    document.getElementById("sitesTable").removeChild(document.getElementById("row" + numOfRows))
    if (numOfRows == 1) {
        document.getElementById("preferencesDeleteRowButton").disabled = true
    }
}
/**
 * Deletes a column in the main table
 */
export function sitesTableDeleteColumn() {
    --numOfColumns
    for (let i = 0; i < numOfRows; ++i) {
        document.getElementById("row" + i).removeChild(document.getElementById("cell" + i + numOfColumns))
    }
    if (numOfColumns == 1) {
        document.getElementById("preferencesDeleteColumnButton").disabled = true
    }
}
/**
 * Changes the main table to an empty table with the given dimensions
 * Is exported to be used at loading the new data (data module)
 * @param {int} rows
 * @param {int} columns
 */
export function setEmptyTableSize(rows, columns) {
    document.getElementById("sitesTable").innerHTML = ""
    numOfRows = 0
    numOfColumns = 0
    // The call to add-row must be done first to create the <tr> tag
    let i
    for (i = 0; i < rows; ++i) {
        sitesTableAddRow()
    }
    for (i = 0; i < columns; ++i) {
        sitesTableAddColumn()
    }
    // The cells just created, update the edit mark if needed
    showEditMark(inEditSiteMode)
}
/**
 * Shows and hide the edit mark of the table
 * Is exported to be used in the main module when showing/hiding preferences
 * @param {Boolean} show Flag that determines if show or hide the mark, if true, the edit mark will show, if false, the edit mark will hide
 */
export function showEditMark(show) {
    inEditSiteMode = show
    for (const editMark of document.getElementsByClassName("editMark")) {
        if (show) {
            editMark.classList.remove("hiddenElement")
            editMark.parentElement.title = getTitleWithEditMark(editMark.parentNode.title)
        } else {
            editMark.classList.add("hiddenElement")
            editMark.parentElement.title = getTitleWithoutEditMark(editMark.parentElement.title)
        }
    }
}
/**
 * Listener function for the click-a-cell event
 */
function cellListener() {
    if (inEditSiteMode) {
        editSiteMenuShow()
        selectedCell = event.currentTarget
        // Fill the edit menu with the values of the selectedCell
        document.getElementById("editSiteHeader").textContent = "(" + selectedCell.id[4] + ", " + selectedCell.id[5] + ")"
        document.getElementById("editSiteName").value = getTitleWithoutEditMark(selectedCell.title)
        document.getElementById("editSiteLink").value = selectedCell.link
        document.getElementById("editSiteImageTypeFile").checked = selectedCell.imageIsFile
        setImageInput(document.getElementById("editSiteImage"), selectedCell.imageIsFile, getImageUrl(selectedCell.style.backgroundImage))
    } else {
        window.location = event.currentTarget.link
    }
}
/**
 * Shows the edit site menu (with an enter animation)
 */
function editSiteMenuShow() {
    document.getElementById("editSite").style.animation = "editSiteAnimationEnter 1s"
    document.getElementById("blockingDiv").classList.toggle("hiddenElement")
    document.body.style.overflow = "hidden"
}
/**
 * Hides the edit site menu (with an exit animation)
 */
function editSiteMenuHide() {
    document.getElementById("editSite").style.animation = "editSiteAnimationExit 0.25s"
    setTimeout(() => {
        // Hide the div only after the animation ends
        document.getElementById("blockingDiv").classList.toggle("hiddenElement")
    }, 250)
    document.body.style.overflow = "auto"
}
/**
 * Performs a cancel site menu
 * Is exported to use it in the main module for the button listener
 */
export function editSiteCancel() {
    editSiteMenuHide()
}
/**
 * Performs a save site menu
 * Is exported to use it in the main module for the button listener
 */
export function editSiteSave() {
    selectedCell.title = getTitleWithEditMark(document.getElementById("editSiteName").value)
    selectedCell.link = document.getElementById("editSiteLink").value
    selectedCell.imageIsFile = document.getElementById("editSiteImageTypeFile").checked
    if (selectedCell.imageIsFile) {
        let input = document.getElementById("editSiteImage")
        let files = input.files
        if (files && files.length) {
            let fileReader = new FileReader()
            fileReader.readAsDataURL(files[0])
            fileReader.onload = () => {
                selectedCell.style.backgroundImage = "url('" + fileReader.result + "')"
                input.value = ""
            }
        }
    } else {
        selectedCell.style.backgroundImage = "url('" + document.getElementById("editSiteImage").value + "')"
    }
    editSiteMenuHide()
}
/**
 * Returns the URL of the currently selected cell
 * Is exported to use it in the main module for filling the image input
 */
export function getSelectedCellImageUrl() {
    if (selectedCell.style.backgroundImage.startsWith("url")) {
        return getImageUrl(selectedCell.style.backgroundImage)
    }
    // The backgroundImage could be a file data, if so, return "" to avoid printing the image data
    return ""
}
/**
 * When editing the table, we want the title of the cells to be "EDIT_MARK: NAME_SITE", this function returns the title with the mark
 * @param {String} title String with the title of a site
 * @returns The given title with the edit mark
 */
function getTitleWithEditMark(title) {
    return browser.i18n.getMessage("editMarkLabel") + ": " + title
}
/**
 * If we are not editing the table or we want to save the table data, we need the title of the cells to be the name of the site (without the edit mark)
 * Is exported to use it in the data module for saving the name site correctly (without the edit mark)
 * @param {String} title String with the title of a site
 * @returns The given title without the edit mark
 */
export function getTitleWithoutEditMark(title) {
    if (title.startsWith(browser.i18n.getMessage("editMarkLabel") + ": ")) {
        return title.substring(browser.i18n.getMessage("editMarkLabel").length + 2)
    }
    return title
}
/**
 * Auxiliar function that will return an URL of a given style.backgroundImage, this is the same string but without "url('" at the beginning and without "')" at the end
 * @param {String} backgroundImageStyle The string of cell.style.backgroundImage
 * @returns The given string without "url('')"
 */
function getImageUrl(backgroundImageStyle) {
    return backgroundImageStyle.substring(5, backgroundImageStyle.length - 2)
}
