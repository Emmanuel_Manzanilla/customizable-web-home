/**
 * Main module, add event listener of every HTML element that need it and defines some general functions for other modules
 */

import { loadData, setLanguageSensibleData, getCurrentData } from "./data.js"
import { sitesTableAddRow, sitesTableAddColumn, sitesTableDeleteRow, sitesTableDeleteColumn, showEditMark, getSelectedCellImageUrl, editSiteCancel, editSiteSave } from "./table.js"

// > ON LOAD
window.onload = () => {
    browser.storage.local.get().then(loadData, handleStorageError)
    setLanguageSensibleData()
}

// > SEARCH FORM LISTENER
document.getElementById("searchForm").onsubmit = () => {
    let link = encodeURIComponent(document.getElementById("searchBar").value)
    let engine = document.getElementById("searchSelector").value
    switch (engine) {
        case "Google":
            link = "https://www.google.es/#q=" + link
            break;
        case "DuckDuckGo":
            link = "https://duckduckgo.com/?q=" + link
            break;
        case "Startpage":
            link = "https://www.startpage.com/row/search?q=" + link
            break;
    }
    window.location = link
}

// > PREFERENCES AREA LISTENERS
document.getElementById("preferencesIcon").onclick = () => {
    togglePreferences()
    showEditMark(true)
}
document.getElementById("preferencesCancelButton").onclick = () => {
    // Cancel, load the data in storage
    browser.storage.local.get().then(loadData, handleStorageError)
    togglePreferences()
    showEditMark(false)
}
document.getElementById("preferencesSaveButton").onclick = () => {
    browser.storage.local.set(getCurrentData())
    togglePreferences()
    showEditMark(false)
}

for (const treeHeader of document.getElementsByClassName("treeHeader")) {
    treeHeader.onclick = () => {
        treeHeader.classList.toggle("treeHeaderOpen")
        treeHeader.nextElementSibling.classList.toggle("hiddenElement")
        // The following element of a treeHeader must be a class treeChild
    }
}

// > PREFERENCES > SITES TABLE LISTENERS
document.getElementById("preferencesAddRowButton").onclick = sitesTableAddRow
document.getElementById("preferencesAddColumnButton").onclick = sitesTableAddColumn
document.getElementById("preferencesDeleteRowButton").onclick = sitesTableDeleteRow
document.getElementById("preferencesDeleteColumnButton").onclick = sitesTableDeleteColumn

// > PREFERENCES > SITES TABLE > EDIT SITE LISTENERS
document.getElementById("editSiteImageTypeFile").onchange = (event) => setImageInput(document.getElementById("editSiteImage"), event.currentTarget.checked, getSelectedCellImageUrl())
document.getElementById("editSiteSaveButton").onclick = editSiteSave
document.getElementById("editSiteCancelButton").onclick = editSiteCancel

// > PREFERENCES > OTHER OPTIONS LISTENERS
document.getElementById("preferencesBodyColor").onchange = (event) => document.body.style.backgroundColor = event.currentTarget.value

document.getElementById("preferencesDefaultSearchSelector").onchange = (event) => document.getElementById("searchSelector").value = event.currentTarget.value

document.getElementById("preferencesCornersSelector").onchange = (event) => setCornersSelector(event.currentTarget.value)
document.getElementById("preferencesCornersRotate").onchange = (event) => setCornersRotation(event.currentTarget.checked)
document.getElementById("preferencesCornersImageTypeFile").onchange = (event) => setImageInput(document.getElementById("preferencesCornersImage"), event.currentTarget.checked)
document.getElementById("preferencesCornersImage").onchange = (event) => {
    let files = event.currentTarget.files
    if (files && files.length) {
        let fileReader = new FileReader()
        fileReader.readAsDataURL(files[0])
        fileReader.onload = () => {
            setCornersImage(fileReader.result, true)
        }
    } else {
        // The input si not a file, is an URL
        setCornersImage(event.currentTarget.value, true)
    }
}

// > PREFERENCES > IMPORT/EXPORT
document.getElementById("preferencesImport").onchange = (event) => {
    let files = event.currentTarget.files
    if (FileReader) {
        let fileReader = new FileReader()
        fileReader.readAsBinaryString(files[0])
        fileReader.onload = () => {
            loadData(JSON.parse(fileReader.result))
        }
    }
}
document.getElementById("preferencesExportButton").onclick = () => {
    let element = document.createElement("a")
    element.href = "data:application/jsoncharset=utf-8," + encodeURIComponent(JSON.stringify(getCurrentData()))
    element.download = (browser.i18n.getMessage("preferencesExportFileName") + ".json")
    document.body.appendChild(element)
    element.click()
    document.body.removeChild(element)
}

// > FUNCTIONS NEEDED IN LISTENERS BELOW
/**
 * Alerts that an error ocurred while loading the data from the browser storage
 * @param {*} error An error produced by browser.storage.local.get()
 */
function handleStorageError(error) {
    alert("Some error ocurred when loading data from storage, see the console")
    console.error(error)
}
/**
 * Toggles the HTML view between the preferences-icon and preferences-fields
 * This function was made to avoid repeating code
 */
function togglePreferences() {
    document.getElementById("preferencesIcon").classList.toggle("hiddenElement")
    document.getElementById("preferencesFields").classList.toggle("hiddenElement")
}
/**
 * Sets an image input to an input-type-text for obtaining the image from an URL or a input-type-file for obtaining the image from a file
 * @param {Object} input HTML input element with the input of the image
 * @param {Boolean} isFile Flag that will determine the type of input, if TRUE, the input will have a type FILE, if FALSE, input will have a type TEXT
 * @param {String} value Optional string with the URL to fill the input with
 */
export function setImageInput(input, isFile, value = "") {
    if (isFile) {
        input.type = "file"
    } else {
        input.type = "text"
        input.value = value
    }
}
/**
 * Changes different HTML elements in the page that need to change if the corners-selector changes
 * Is exported to be used at loading the data from browser storage
 * @param {String} value Value of the corners selector
 */
export function setCornersSelector(value) {
    let corners = [
        document.getElementById("cornerTL"),
        document.getElementById("cornerTR"),
        document.getElementById("cornerBL"),
        document.getElementById("cornerBR")
    ]
    switch (value) {
        case "default":
            document.getElementById("preferencesCornersCustomOptions").classList.add("hiddenElement")
            for (const corner of corners) {
                corner.classList.add("defaultCornersDecoration")
                corner.innerHTML = ""
            }
            break;
        case "custom":
            document.getElementById("preferencesCornersCustomOptions").classList.remove("hiddenElement")
            for (const corner of corners) {
                corner.classList.remove("defaultCornersDecoration")
                corner.innerHTML = ""
            }
            break;
        case "none":
            document.getElementById("preferencesCornersCustomOptions").classList.add("hiddenElement")
            for (const corner of corners) {
                corner.classList.remove("defaultCornersDecoration")
                corner.innerHTML = ""
            }
            break;
    }
}
/**
 * Changes different HTML elements in the page that need to change if the rotate-corners-checkbox changes
 * This is not implemented as a anonymous function in the onchange event because when a corners-image is loaded, this function is needed
 * @param {Boolean} value Value of the rotate corners checkbox
 */
function setCornersRotation(value) {
    let corners = [
        document.getElementById("cornerTR"),
        document.getElementById("cornerBL"),
        document.getElementById("cornerBR")
    ]
    if (corners[0].hasChildNodes()) {
        if (value) {
            corners[0].children[0].classList.add("rotationTR")
            corners[1].children[0].classList.add("rotationBL")
            corners[2].children[0].classList.add("rotationBR")
        } else {
            corners[0].children[0].classList.remove("rotationTR")
            corners[1].children[0].classList.remove("rotationBL")
            corners[2].children[0].classList.remove("rotationBR")
        }
    }
    // If there are no child nodes, you cannot control rotation
}

/**
 * Sets the corners-image from a given string of data
 * This function calls the function setCornersRotation at to refresh the new setted image
 * Is exported to be used at loading the data from browser storage
 * @param {String} imageData Image data as string ("url" or "data:image/...")
 */
export function setCornersImage(imageData) {
    let corners = [
        document.getElementById("cornerTL"),
        document.getElementById("cornerTR"),
        document.getElementById("cornerBL"),
        document.getElementById("cornerBR")
    ]
    let image = document.createElement("img")
    image.src = imageData
    if (corners[0].hasChildNodes()) {
        corners[0].replaceChild(image, corners[0].children[0])
        for (let i = 1; i < corners.length; ++i) {
            corners[i].replaceChild(image.cloneNode(), corners[i].children[0])
        }
    } else {
        corners[0].appendChild(image)
        for (let i = 1; i < corners.length; ++i) {
            corners[i].appendChild(image.cloneNode())
        }
    }
    setCornersRotation(document.getElementById("preferencesCornersRotate").checked)
}
