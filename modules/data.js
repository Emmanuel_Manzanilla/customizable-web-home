/**
 * Data module, define functions to manage the data of the application
 */

import { numOfRows, numOfColumns, setEmptyTableSize, getTitleWithoutEditMark } from "./table.js"
import { setImageInput, setCornersSelector, setCornersImage } from "./main.js"

/**
 * Load a given data to the page, if the given data is not valid, it loads the default data
 * Is exported to use it in the main module (onload, cancel and import)
 * @param {Object} data The data to load
 */
export function loadData(data) {
    if (data && data["numOfRows"] > 0 && data["numOfColumns"] > 0) {
        // Fill inputs with the data
        document.getElementById("preferencesBodyColor").value = data["backgroundColor"]

        document.getElementById("preferencesDefaultSearchSelector").value = data["defaultSearchEngine"]
        document.getElementById("searchSelector").value = data["defaultSearchEngine"]

        document.getElementById("preferencesCornersSelector").value = data["cornersType"]
        document.getElementById("preferencesCornersRotate").checked = data["cornersImageRotated"]
        let checkbox = document.getElementById("preferencesCornersImageTypeFile");
        if (data["cornersImage"].startsWith("data")) {
            checkbox.checked = true;
        } else {
            checkbox.checked = false;
        }
        setImageInput(document.getElementById("preferencesCornersImage"), checkbox.checked, data["cornersImage"])

        // Fill other parts of the page with the data
        setEmptyTableSize(data["numOfRows"], data["numOfColumns"])
        let cell
        for (const [key, value] of Object.entries(data["cells"])) {
            cell = document.getElementById(key)
            cell.title = value["name"]
            cell.link = value["link"]
            cell.imageIsFile = value["imageIsFile"]
            cell.style.backgroundImage = value["image"]
        }
        document.body.style.backgroundColor = data["backgroundColor"]
        setCornersSelector(data["cornersType"])
        if (data["cornersType"] == "custom") {
            setCornersImage(data["cornersImage"])
        }
    } else {
        // Recursive call with the default data
        loadData(getDefaultData())
    }
}
/**
 * Returns the current data of the page
 * Is exported to use it on the main module (save and export)
 * @returns {Object} Object with the current data of the page
 */
export function getCurrentData() {
    let data = {}
    data["numOfRows"] = numOfRows
    data["numOfColumns"] = numOfColumns
    data["cells"] = {}
    for (const cell of document.querySelectorAll("[id^=cell]")) {
        data["cells"][cell.id] = {
            name: getTitleWithoutEditMark(cell.title),
            link: cell.link,
            imageIsFile: cell.imageIsFile,
            image: cell.style.backgroundImage
        }
    }

    data["backgroundColor"] = document.getElementById("preferencesBodyColor").value
    data["defaultSearchEngine"] = document.getElementById("preferencesDefaultSearchSelector").value

    data["cornersType"] = document.getElementById("preferencesCornersSelector").value
    data["cornersImageRotated"] = false
    data["cornersImage"] = ""
    if (data["cornersType"] == "custom") {
        let aCorner = document.getElementById("cornerTL")
        if (aCorner.hasChildNodes()) {
            data["cornersImageRotated"] = document.getElementById("preferencesCornersRotate").checked
            data["cornersImage"] = document.getElementById("cornerTL").children[0].src
        }
    }
    return data
}
/**
 * @returns {Object} Object with a default data
 */
function getDefaultData() {
    let data = {}
    data["numOfRows"] = 2
    data["numOfColumns"] = 4
    data["cells"] = {}

    data["cells"]["cell00"] = {}
    data["cells"]["cell00"]["name"] = "Gmail"
    data["cells"]["cell00"]["link"] = "https://mail.google.com/mail/"
    data["cells"]["cell00"]["imageIsFile"] = false
    data["cells"]["cell00"]["image"] = "url('https://www.cybernautas.es/wp-content/uploads/2008/09/gmail-logo-portada.jpg')"

    data["cells"]["cell01"] = {}
    data["cells"]["cell01"]["name"] = "Google images"
    data["cells"]["cell01"]["link"] = "https://www.google.es/imghp"
    data["cells"]["cell01"]["imageIsFile"] = false
    data["cells"]["cell01"]["image"] = "url('https://upload.wikimedia.org/wikipedia/commons/thumb/7/77/Google_Images_2015_logo.svg/1200px-Google_Images_2015_logo.svg.png')"

    data["cells"]["cell02"] = {}
    data["cells"]["cell02"]["name"] = "Google translate"
    data["cells"]["cell02"]["link"] = "https://translate.google.com/"
    data["cells"]["cell02"]["imageIsFile"] = false
    data["cells"]["cell02"]["image"] = "url('https://i1.wp.com/www.argotrans.com/wp-content/uploads/2019/04/google_translate_logo.jpg?resize=750%2C445&ssl=1')"

    data["cells"]["cell03"] = {}
    data["cells"]["cell03"]["name"] = "YouTube"
    data["cells"]["cell03"]["link"] = "https://www.youtube.com/"
    data["cells"]["cell03"]["imageIsFile"] = false
    data["cells"]["cell03"]["image"] = "url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgIrMr8c79DKVeQfg27pvUNVQ7Jkv6dXAcL6NSlFZbQYqSh0E5Og&s')"

    data["cells"]["cell10"] = {}
    data["cells"]["cell10"]["name"] = "Gitlab"
    data["cells"]["cell10"]["link"] = "https://gitlab.com/"
    data["cells"]["cell10"]["imageIsFile"] = false
    data["cells"]["cell10"]["image"] = "url('https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-rgb.png')"

    data["cells"]["cell11"] = {}
    data["cells"]["cell11"]["name"] = "Github"
    data["cells"]["cell11"]["link"] = "https://github.com/"
    data["cells"]["cell11"]["imageIsFile"] = false
    data["cells"]["cell11"]["image"] = "url('https://i.pinimg.com/600x315/2c/b6/70/2cb670b6ddd8922a1c1b2fee4f6f758c.jpg')"

    data["cells"]["cell12"] = {}
    data["cells"]["cell12"]["name"] = "Amazon"
    data["cells"]["cell12"]["link"] = "https://www.amazon.com/"
    data["cells"]["cell12"]["imageIsFile"] = false
    data["cells"]["cell12"]["image"] = "url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWY6_eXLVqMxnEuH0mSMe4jSpRH4EdHy6LxJH5biZcOQ0jW3mi&s')"

    data["cells"]["cell13"] = {}
    data["cells"]["cell13"]["name"] = "Twitch"
    data["cells"]["cell13"]["link"] = "https://www.twitch.tv/"
    data["cells"]["cell13"]["imageIsFile"] = false
    data["cells"]["cell13"]["image"] = "url('https://n-economia.com/wp-content/uploads/twitch.png')"

    data["backgroundColor"] = "#f1f1f1"
    data["defaultSearchEngine"] = "Google"
    data["cornersType"] = "default"
    data["cornersImage"] = ""
    data["cornersImageRotated"] = false
    return data
}
/**
 * Sets language sensible data with the i18n API for firefox extension
 * Is exported to use it on the main module (onload)
 */
export function setLanguageSensibleData() {
    document.title = browser.i18n.getMessage("pageTitle")

    document.getElementById("searchBar").title = browser.i18n.getMessage("searchBarTitle")
    document.getElementById("searchSelector").title = browser.i18n.getMessage("searchSelectorTitle")
    let foo
    foo = document.getElementById("searchSubmit")
    foo.title = foo.textContent = browser.i18n.getMessage("searchSubmitLabel")

    foo = document.getElementById("editSiteNameLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("editSiteNameLabel")
    foo = document.getElementById("editSiteLinkLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("editSiteLinkLabel")
    foo = document.getElementById("editSiteImageTypeFileLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("commonLabelImageTypeFile")
    foo = document.getElementById("editSiteCancelButton")
    foo.title = foo.textContent = browser.i18n.getMessage("editSiteCancelLabel")
    foo = document.getElementById("editSiteSaveButton")
    foo.title = foo.textContent = browser.i18n.getMessage("editSiteSaveLabel")

    foo = document.getElementById("preferencesSectionTable")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesSectionTableLabel")
    foo = document.getElementById("preferencesAddRowButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesAddRowLabel")
    foo = document.getElementById("preferencesAddColumnButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesAddColumnLabel")
    foo = document.getElementById("preferencesDeleteRowButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesDeleteRowLabel")
    foo = document.getElementById("preferencesDeleteColumnButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesDeleteColumnLabel")

    foo = document.getElementById("preferencesSectionOthers")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesSectionOthersLabel")
    foo = document.getElementById("preferencesBodyColorLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesBodyColorLabel")
    foo = document.getElementById("preferencesDefaultSearchSelectorLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesDefaultSearchSelectorLabel")
    foo = document.getElementById("preferencesCornersSelectorLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCornersSelectorLabel")
    foo = document.getElementById("preferencesCornersModeDefault")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCornersModeDefaultLabel")
    foo = document.getElementById("preferencesCornersModeCustom")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCornersModeCustomLabel")
    foo = document.getElementById("preferencesCornersModeNone")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCornersModeNoneLabel")
    foo = document.getElementById("preferencesCornersImageTypeFileLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("commonLabelImageTypeFile")
    foo = document.getElementById("preferencesCornersRotateLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCornersRotateLabel")

    foo = document.getElementById("preferencesSectionImportExport")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesSectionImportExportLabel")
    foo = document.getElementById("preferencesImportLabel")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesImportLabel")
    foo = document.getElementById("preferencesExportButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesExportLabel")

    foo = document.getElementById("preferencesCancelButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesCancelLabel")
    foo = document.getElementById("preferencesSaveButton")
    foo.title = foo.textContent = browser.i18n.getMessage("preferencesSaveLabel")
}
