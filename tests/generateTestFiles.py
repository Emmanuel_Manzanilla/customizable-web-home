#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""
Generates two files an HTML and a JS:
	- The JS file contains all of the modules of the extension appended in one file, this make some test easier, for example calling functions from the browser console
	- The HTML file is a copy of the extension index.html but it changes the location of some elements (for example it uses the generated JS)

How to use the script:
	1: Execute it to generate the tests files:
		`python generateFunctionsFile.py`
	2: Open the generated index.html in this directory to make tests

WARNING: This script was created to fit this project, it DOES NOT WORK in general for any extension that uses JS modules
"""

if __name__== "__main__":
	outFileJS = open("functions.js","w+")
	languageFunction = "browser.i18n.getMessage("
	storageGetFunction = "browser.storage.local.get("
	storageSetFunction = "browser.storage.local.set("
	modules = ["../modules/table.js", "../modules/data.js", "../modules/main.js"]
	for m in modules:
		file2Read = open(m, "r")
		lines = file2Read.readlines()
		for line in lines:
			if (line.startswith("import")):
				# Replace imports for a comment
				line = "// import omitted: " + line
			elif (line.startswith("export")):
				# Remove export keyword only AT THE BEGINING OF A LINE
				line = line[len("export "):]
			else:
				# Replace the call to a getMessage (extension only) for the the representing string
				index = line.find(languageFunction)
				if (index != -1):
					openingParenthesis = index + len(languageFunction)
					closingParenthesis = line[openingParenthesis:].find(")") + openingParenthesis
					line = line[0:index] + line[openingParenthesis:closingParenthesis] + line[closingParenthesis + 1:] + "// browser.i18n.getMessage omitted \n"
				else:
					# Replace the call to the get storage function (extension only) for a call to loadData(getDefaultData())
					index = line.find(storageGetFunction)
					if (index != -1):
						line = "    loadData(getDefaultData()) // changed from browser.storage.get\n"
					else:
						# Replace the call to the set storage function (extension only) for a comment
						index = line.find(storageSetFunction)
						if (index != -1):
							line = "    // browser.storage.set omitted\n"
			outFileJS.write(line)
		file2Read.close()
	outFileJS.close()

	outFileHTML = open("index.html","w+")
	typeModuleString = 'type="module"'
	file2Read = open("../index.html", "r")
	lines = file2Read.readlines()
	for line in lines:
		index = line.find("modules/main.js")
		if (index != -1):
			line = '    <script src="functions.js" defer></script>'
		else:
			index = line.find("styles.css")
			if(index != -1):
				line = line.replace("styles.css", "../styles.css")
			else:
				index = line.find("images/")
				if (index != -1):
					line = line.replace("images/", "../images/")
		outFileHTML.write(line)
	file2Read.close()
	outFileHTML.close()

