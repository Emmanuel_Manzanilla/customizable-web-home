
[English version of this file](../../Readme.md)

# Inicio web personalizable

Extensión de firefox que cambia la página de inicio y la página de pestaña nueva a una pagina web personalizable

Esta extensión pide los siguientes permisos:
- `almacenamiento` : para guardar la información personalizable de la página
- `almacenamiento ilimitado` : para no limitar la cantidad de información que la extensión puede guardar. En esta extensión es posible añadir imágenes desde archivos a la página personalizable, para no limitar el tamaño de estos archivos este permiso de almacenamiento ilimitado es necesario

El código fuente de esta extensión se puede encontrar en [este repositorio de gitlab](https://gitlab.com/juanorith/customizable-web-home), si quieres contribuir a este proyecto ¡no dudes en hacerlo!

Este software está licenciado bajo la Licencia Pública General de GNU versión 2 [ver el archivo de la licencia para más detalles](../../License.txt)

Las imágenes empaquetadas en este software son originales y licenciadas bajo  Creative Commons Attribution 4.0 International (CC BY 4.0) [ver el archivo de la licencia de imágenes para más detalles](../../images/License_images.txt)
